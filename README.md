# CVLibrary

[![CI Status](https://img.shields.io/travis/Cesar Velasquez/CVLibrary.svg?style=flat)](https://travis-ci.org/Cesar Velasquez/CVLibrary)
[![Version](https://img.shields.io/cocoapods/v/CVLibrary.svg?style=flat)](https://cocoapods.org/pods/CVLibrary)
[![License](https://img.shields.io/cocoapods/l/CVLibrary.svg?style=flat)](https://cocoapods.org/pods/CVLibrary)
[![Platform](https://img.shields.io/cocoapods/p/CVLibrary.svg?style=flat)](https://cocoapods.org/pods/CVLibrary)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CVLibrary is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CVLibrary'
```

## Author

Cesar Velasquez, cvelasqh@everis.com

## License

CVLibrary is available under the MIT license. See the LICENSE file for more info.
